local larguraTela = love.graphics.getWidth()
local alturaTela = love.graphics.getHeight()

function love.load()
    --carregando a imagem na memoria
    --love.graphics.setBackgroundColor(255, 255, 255)

    --nave
    imgNave = love.graphics.newImage('/Naves/nave_hero.png')
    nave = {
      posX = larguraTela / 2,
      posY = alturaTela-90,
      velocidade = 300
    }
    --nave

    --tiro
    atira = true
    delayTiro = 0.1
    tempoAteAtirar = delayTiro
    tiros = {}
    imgTiro = love.graphics.newImage('/tiro/tiro__bf.png')
    --tiro

    --inimigos
    gateEnemyDraw = false
    delayInimigo = 0.05
    tempoCriarInimigo = delayInimigo
    inimigos = {}
    imgInimigo = love.graphics.newImage('nave_inimiga.png')
    --inimigos

  --[[  --sargentos
    delayInimigo2 = 0.05
    tempoCriarInimigo2 = delayInimigo2
    inimigos2 = {}
    imgInimigo2 = love.graphics.newImage('nave_inimiga.png')
    --sargentos]]--

    --chefao
    delayTempo = 4 --delay até aparecer o inimigo
    tempo = delayTempo --tempo que controlará a entrada da frase e a entrada do chefão
    mostraChefao = false
    gate = 10 --gatilho para chefão entrar (pontos necessários para chegar ao chefao)

    chefaoImg = love.graphics.newImage("boss.png")
    chefao = {
      x = larguraTela/2 - chefaoImg:getWidth()/2,
      y = 0,
      vel = 30
    }
    --frase do chefao
    palavras = {
      love.graphics.print("BOSS", 20, alturaTela /2 ),
      love.graphics.print("BOSS",  80, alturaTela /2 ),
      love.graphics.print("BOSS",  140, alturaTela /2 ),
      love.graphics.print("BOSS", 200, alturaTela /2 ),
      love.graphics.print("BOSS", 280, alturaTela /2 ),
      love.graphics.print("BOSS", 360, alturaTela /2 ),
      love.graphics.print("BOSS", 420, alturaTela /2 ),
      love.graphics.print("BOSS", 500, alturaTela /2 ),
      love.graphics.print("BOSS", 580, alturaTela /2 ),
      love.graphics.print("BOSS", 650, alturaTela /2 ),
      love.graphics.print("BOSS", 720, alturaTela /2 )
  }
    --frase do chefao
    --chefao

    --vidas
    estaVivo = true
    pontos = 0
    --vidas

    --Background
    fundo = love.graphics.newImage("espaco_sideral.png")
    fundo2 = love.graphics.newImage("espaco_sideral.png")

    planoDeFundo = {
      x = 0,
      y = 0,
      y2 = 0 - fundo:getHeight(),
      vel = 30
    }
    --Background

    --fonte
    fonte = love.graphics.newImageFont("font.png",    " abcdefghijklmnopqrstuvwxyz" ..    "ABCDEFGHIJKLMNOPQRSTUVWXYZ0" ..    "123456789.,!?-+/():;%&`'*#=[]\"")
    --fonte

    --sons
    somDoTiro = love.audio.newSource("Sons/tiro_new_6db.mp3", "static")
    --explodeNave = love.audio.newSource("ExplosaoNave.mp3", "static")
    --explodeInimigo = love.audio.newSource("ExplosaoInimigo.mp3", "static")
    musica = love.audio.newSource("Sons/musica.mp3", "static")
    musica:play()
    musica:setLooping(true)
    --sons

    --Tela inicial
    abreTela = false
    --Tela inicial
end

function love.update(dt)
    atirar(dt)
    enemy(dt)
    movimentos(dt)
    colisoes()
    reset()
    planoDeFundoScrolling(dt)
end

function movimentos(dt)
  --nave pra direita
  if love.keyboard.isDown('right') then
  if nave.posX < (larguraTela - imgNave:getWidth() / 2)-25.2 then
      nave.posX = nave.posX + (nave.velocidade * dt)
  end
  end
  --nave pra direita

  if love.keyboard.isDown('left') then
  if nave.posX > (0 - imgNave:getWidth() / 2)+25.2 then
      nave.posX = nave.posX - (nave.velocidade * dt) --- movimenta a nave
  end
  end

  if love.keyboard.isDown('up') then
  if nave.posY > (0 - imgNave:getHeight() / 2)+40 then
      nave.posY = nave.posY - (nave.velocidade * dt) --- movimenta a nave
  end
  end
  if love.keyboard.isDown('down') then
  if nave.posY < (alturaTela - imgNave:getHeight() / 2)-50 then
      nave.posY = nave.posY + (nave.velocidade * dt) --- movimenta a nave
  end
  end
end

function enemy(dt)
  --condição para desenhar os inimigos na tela
    if pontos < gate then
      gateEnemyDraw = true
    end
  --condição para desenhar os inimigos na tela

  --condição para desenhar soldados aparecem na tela
    if gateEnemyDraw and not mostraChefao then
        tempoCriarInimigo = tempoCriarInimigo - (1 * dt)
        if tempoCriarInimigo < 0 then
          tempoCriarInimigo = delayInimigo
          numeroAleatorio = math.random(10, love.graphics.getWidth()-((imgInimigo:getWidth()/2)+10 )) --delimita o intervalo inicial e final no qual as naves inimigas apareceram x0 e x1
          novoInimigo = {x = numeroAleatorio, y = imgInimigo:getWidth(), img = imgInimigo } --atribui os valores x, y e imagem
          table.insert(inimigos, novoInimigo) -- insere na tabela inimigos a tabela com novos inimigos no caso a variável"novoInimigo."
        end
        for i, inimigo in ipairs ( inimigos ) do
          inimigo.y = inimigo.y + (200 * dt)
          if inimigo.y > 850 then
            table.remove(inimigos, i)
          end
        end
    end
  -- soldados aparecem na tela

  -- chefão aparece na tela e soldados somem
    if pontos >= gate then
      mostraChefao = true
      tempo = tempo - (1 * dt) --delay até que o chefao aapareça

      --chefão aparece na Tela
      chefao.y = -300
      --chefao aparece na tela

      --naves saem de dentro da nave mae
      if tempo < 0 then
      tempoCriarInimigo = tempoCriarInimigo - (1 * dt)
        if tempoCriarInimigo < 0 then
          tempoCriarInimigo = delayInimigo
          numeroAleatorio2 = math.random(chefaoImg:getWidth()-350, chefaoImg:getWidth())--gera um valor x aleatorio
          novoInimigo2 = {x = numeroAleatorio2, y = imgInimigo:getWidth(), img = imgInimigo } --atribui os valores x,  y e imagem
          table.insert(inimigos, novoInimigo2) -- insere na tabela inimigos a tabela com novos inimigos no caso a variável"novoInimigo."
        end
        -- remove os inimigos que ultrapassao o limite da tela
        for i, inimigo in ipairs ( inimigos ) do
          inimigo.y = inimigo.y + (200 * dt)
          if inimigo.y > 850 then
            table.remove(inimigos, i)
          end
        end
        -- remove os inimigos que ultrapassao o limite da tela
      end
    --naves saem de dentro da nave mae
    end
  -- chefão aparece na tela e soldados somem
end

function atirar(dt)
    --------------Tiro
    tempoAteAtirar = tempoAteAtirar - (1 * dt)
    if tempoAteAtirar < 0 then
        atira = true
    end

    if estaVivo then
      if love.keyboard.isDown('q') and atira then
          novoTiro = {x = nave.posX+25, y = nave.posY, img = imgTiro}
          table.insert(tiros, novoTiro)
          somDoTiro:stop()
      	  somDoTiro:play()
          atira = false
        tempoAteAtirar = delayTiro
      end
    end



    for i, tiro in ipairs(tiros) do
        tiro.y = tiro.y - (500 * dt)
        if tiro.y < 0 then
            table.remove(tiros, i)
        end
    end
end

function love.draw()
    --Background
    love.graphics.draw(fundo, planoDeFundo.x, planoDeFundo.y)
    love.graphics.draw(fundo2, planoDeFundo.x, planoDeFundo.y2)
    --Background

    for i, tiro in ipairs(tiros) do
        love.graphics.draw(tiro.img, tiro.x, tiro.y, 0, 1, 1, imgTiro:getWidth() / 2, imgTiro:getHeight())
    end

    --pontos na tela
    love.graphics.setFont(fonte)
    love.graphics.print("Pontuacao: "..pontos, 10, 10)
    --pontos na tela

    --Game over e Reset
    if estaVivo then
        love.graphics.draw(imgNave, nave.posX, nave.posY)
    else
        love.graphics.print("Aperte 'r' para reiniciar", larguraTela / 3, alturaTela /2 )
    end
    --Game over e Reset

    if mostraChefao then
        if tempo > 0 then --tempo pra frase sumir da tela
          for i, inimigo in ipairs ( inimigos ) do
              table.remove(inimigos, i)
            end
          love.graphics.print("BOSS", 20, alturaTela /2 )
          love.graphics.print("BOSS", 80, alturaTela /2 )
          love.graphics.print("BOSS", 140, alturaTela /2)
          love.graphics.print("BOSS", 200, alturaTela /2 )
          love.graphics.print("BOSS", 280, alturaTela /2 )
          love.graphics.print("BOSS", 360, alturaTela /2 )
          love.graphics.print("BOSS", 420, alturaTela /2 )
          love.graphics.print("BOSS", 500, alturaTela /2 )
          love.graphics.print("BOSS", 580, alturaTela /2 )
          love.graphics.print("BOSS", 650, alturaTela /2 )
          love.graphics.print("BOSS", 720, alturaTela /2 )
        end
      if tempo < 0 then
        gateEnemyDraw = true
        love.graphics.draw(chefaoImg, chefao.x, chefao.y)
      end
    end
    --ininmigos
    if gateEnemyDraw then
      for i, inimigo in ipairs (inimigos) do
        love.graphics.draw(inimigo.img, inimigo.x, inimigo.y)
      end
    end
    --inimigos

end

function colisoes()
  for i, inimigo in ipairs(inimigos) do
    for j, tiro in ipairs(tiros) do
      if checaColisao(inimigo.x, inimigo.y, imgInimigo:getWidth(), imgInimigo:getHeight(), tiro.x, tiro.y, imgTiro:getWidth(), imgTiro:getHeight()) then
          table.remove(tiros, j)
          table.remove(inimigos, i)
        --  explodeInimigo:stop()
        --  explodeInimigo:play()
          pontos = pontos + 1
        end
    end

      --checa morte da nave_her

    if checaColisao(inimigo.x, inimigo.y, imgInimigo:getWidth(), imgInimigo:getHeight(), nave.posX, nave.posY + 20, imgNave:getWidth(), imgNave:getHeight()) and estaVivo
      then
        --( imgNave:getWidth() / 2)
        table.remove(inimigos, i)
        --explodeNave:play()
        estaVivo = false
    end
      --checa morte da nave_hero


    if not gateEnemyDraw then
      table.remove(inimigos, i)
    end
  end
end

function checaColisao(x1, y1, w1, h1, x2, y2, w2, h2)
  return x1 < x2 + w2 and x2 < x1 + w1 and y1 < y2 + h2 and y2 < y1 + h1
end

function reset ()
  if not estaVivo and love.keyboard.isDown('r') then
    tiros = {}
    inimigos = {}

    atira = tempoAteAtirar
    tempoCriarInimigo = delayInimigo

    posX = larguraTela / 2
    posY = alturaTela-90

    pontos = 0
    estaVivo = true
    mostraChefao = false

    tempo = delayTempo

  end
end

function planoDeFundoScrolling (dt)
  planoDeFundo.y = planoDeFundo.y + planoDeFundo.vel * dt
  planoDeFundo.y2 = planoDeFundo.y2 + planoDeFundo.vel * dt

  if planoDeFundo.y > alturaTela then
    planoDeFundo.y = planoDeFundo.y2 - fundo2:getHeight()
  end

  if planoDeFundo.y2 > alturaTela then
    planoDeFundo.y2 = planoDeFundo.y - fundo:getHeight()
  end
end
