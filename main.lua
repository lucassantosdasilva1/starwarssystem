local larguraTela = love.graphics.getWidth()
local alturaTela = love.graphics.getHeight()
local win = false


function love.load()
  --MENSAGEM DE VENCEDOR
    win = false
    winImg = love.graphics.newImage("youwin.png")
    criterioVencer = 100
  --MENSAGEM DE VENCEDOR

    --carregando a imagem na memoria
    --nave
    imgNave = love.graphics.newImage('/Naves/nave_hero.png') --img da nave
    nave = {
      posX = larguraTela / 2,
      posY = alturaTela-90,
      velocidade = 300
    }
    --nave

    --tiro
    atira = true
    delayTiro = 0.1
    tempoAteAtirar = delayTiro
    tiros = {}
    imgTiro = love.graphics.newImage('/tiro/tiro.png')
    --tiro

    --inimigos
    gateEnemyDraw = false --gatilho que permite desenhar os inimigos no love.draw
    delayInimigo = 0.05 --atraso para criar inimigos
    tempoCriarInimigo = delayInimigo --variavel que recebe atraso para criar inimigos
    inimigos = {} -- tabela que receberá as características do soldado/inimigo (x, y e img)
    imgInimigo = love.graphics.newImage('nave_inimiga.png') -- img da nave soldado/inimigo
    --inimigos

    --chefao
    delayChefao = 2 --delay até o chefão reaparecer na tela
    tempoCriarChefao = delayChefao --tempo que leva pro chefao reaparecer na tela

    delayTempo = 4 --delay até aparecer o chefão pela primeira vez depois da frase
    tempo = delayTempo --tempo que controlará a entrada da frase e autorizará a entrada do chefão
    mostraChefao = false --variavel que controlará o draw do chefão e tbm o desaparecimento dos soldados
    gate = 10 --gatilho para chefão entrar (pontos necessários para chegar ao chefao)

    chefaoImg = love.graphics.newImage("boss.png") --img do chefao
    chefao = {
      x = 0,
      y = 0,
      vel = 30
    } -- lista com as características do chefao
      --frase do chefao
    palavras = love.graphics.newImage("plv.png") --palavra BOSS que aparece quando se alcança o chefao
      --frase do chefao
    --chefao

    --vidas
    estaVivo = true --variavel que serve de controle para dizer se a nave_hero está viva ou não
    pontos = 0 --variavel que recebe a pontuação geral
    --vidas

    --Background
    fundo = love.graphics.newImage("espaco_sideral.png")--img1 do fundo
    fundo2 = love.graphics.newImage("espaco_sideral.png")-- img2 do fundo

    planoDeFundo = {
      x = 0,
      y = 0,
      y2 = 0 - fundo:getHeight(),
      vel = 30
    } --lista que insere os atributos ao plano de fundo
    --Background

    --fonte
    fonte = love.graphics.newImageFont("font.png",    " abcdefghijklmnopqrstuvwxyz" ..    "ABCDEFGHIJKLMNOPQRSTUVWXYZ0" ..    "123456789.,!?-+/():;%&`'*#=[]\"")
    --fonte

    --sons
    somDoTiro = love.audio.newSource("Sons/tiro_new_6db.mp3", "static")
    --explodeNave = love.audio.newSource("ExplosaoNave.mp3", "static")
    --explodeInimigo = love.audio.newSource("ExplosaoInimigo.mp3", "static")
    musica = love.audio.newSource("Sons/musica.mp3", "static")
    musica:play()
    musica:setLooping(true)
    --sons

    --Tela inicial
    abreTela = false
    --Tela inicial
end

function love.update(dt)
    atirar(dt)
    enemy(dt)
    movimentos(dt)
    colisoes()
    reset()
    planoDeFundoScrolling(dt)
end

function movimentos(dt)
  --nave pra direita
  if love.keyboard.isDown('right') then
  if nave.posX < (larguraTela - imgNave:getWidth() / 2)-25.2 then
      nave.posX = nave.posX + (nave.velocidade * dt)
  end
  end
  --nave pra direita

  if love.keyboard.isDown('left') then
  if nave.posX > (0 - imgNave:getWidth() / 2)+25.2 then
      nave.posX = nave.posX - (nave.velocidade * dt) --- movimenta a nave
  end
  end

  if love.keyboard.isDown('up') then
  if nave.posY > (0 - imgNave:getHeight() / 2)+40 then
      nave.posY = nave.posY - (nave.velocidade * dt) --- movimenta a nave
  end
  end
  if love.keyboard.isDown('down') then
  if nave.posY < (alturaTela - imgNave:getHeight() / 2)-50 then
      nave.posY = nave.posY + (nave.velocidade * dt) --- movimenta a nave
  end
  end
end

function enemy(dt)
  --condição para desenhar os inimigos na tela
    if pontos < gate then
      gateEnemyDraw = true
    end
  --condição para desenhar os inimigos na tela

  --condição para desenhar soldados aparecem na tela
    if gateEnemyDraw and not mostraChefao then
        tempoCriarInimigo = tempoCriarInimigo - (1 * dt)
        if tempoCriarInimigo < 0 then
          tempoCriarInimigo = delayInimigo
          numeroAleatorio = math.random(10, love.graphics.getWidth()-((imgInimigo:getWidth()/2)+10 )) --delimita o intervalo inicial e final no qual as naves inimigas apareceram x0 e x1
          novoInimigo = {x = numeroAleatorio, y = imgInimigo:getWidth(), img = imgInimigo } --atribui os valores x, y e imagem
          table.insert(inimigos, novoInimigo) -- insere na tabela inimigos a tabela com novos inimigos no caso a variável"novoInimigo."
        end
        for i, inimigo in ipairs ( inimigos ) do
          inimigo.y = inimigo.y + (200 * dt)
          if inimigo.y > 850 then
            table.remove(inimigos, i)
          end
        end
    end
  -- soldados aparecem na tela

    -- ----------chefão aparece na tela e soldados somem-----------------------------------
    if pontos >= gate then
      mostraChefao = true
      tempo = tempo - (1 * dt) --delay até que o chefao aapareça

      --chefão aparece na Tela
      chefao.y = -300
      tempoCriarChefao = tempoCriarChefao - (1 * dt)
      if tempoCriarChefao < 0 then
        tempoCriarChefao = delayChefao
        numeroAleatorio3 = math.random(0, love.graphics.getWidth()-((chefaoImg:getWidth()/2)+175)) --delimita o intervalo inicial e final no qual as naves inimigas apareceram x0 e x1
        --if numeroAleatorio < (larguraTela - imgNave:getWidth() / 2)-25.2 then
          chefao.x = numeroAleatorio3
        --end
      end

      --chefao aparece na tela

      --naves saem de dentro da nave mae
      if tempo < 0 then
      tempoCriarInimigo = tempoCriarInimigo - (0.5 * dt)
        if tempoCriarInimigo < 0 then
          tempoCriarInimigo = delayInimigo
          numeroAleatorio2 = math.random(chefao.x+85, chefao.x+425)--gera um valor x aleatorio (delimitado entre os limites da nave(parece que ta saindo da nave))
          novoInimigo2 = {x = numeroAleatorio2, y = imgInimigo:getWidth(), img = imgInimigo } --atribui os valores x,  y e imagem
          table.insert(inimigos, novoInimigo2) -- insere na tabela inimigos a tabela com novos inimigos no caso a variável"novoInimigo."
        end
        -- remove os inimigos que ultrapassao o limite da tela
        for i, inimigo in ipairs ( inimigos ) do
          inimigo.y = inimigo.y + (200 * dt)
          if inimigo.y > 850 then
            table.remove(inimigos, i)
          end
        end
        -- remove os inimigos que ultrapassao o limite da tela

        --naves soldados
        tempoCriarInimigo = tempoCriarInimigo - (1 * dt)
        if tempoCriarInimigo < 0 then
          tempoCriarInimigo = delayInimigo
          numeroAleatorio = math.random(10, love.graphics.getWidth()-((imgInimigo:getWidth()/2)+10 )) --delimita o intervalo inicial e final no qual as naves inimigas apareceram x0 e x1
          novoInimigo = {x = numeroAleatorio, y = imgInimigo:getWidth(), img = imgInimigo } --atribui os valores x, y e imagem
          table.insert(inimigos, novoInimigo) -- insere na tabela inimigos a tabela com novos inimigos no caso a variável"novoInimigo."
        end
        for i, inimigo in ipairs ( inimigos ) do
          inimigo.y = inimigo.y + (200 * dt)
          if inimigo.y > 850 then
            table.remove(inimigos, i)
          end
        end
        --naves soldados
      end
    --naves saem de dentro da nave mae

    end

    if pontos == 100 then
      win = true
    end
                      -- ----------chefão aparece na tela e soldados somem-----------------------------------
end

function atirar(dt)
    --------------Tiro
    if not mostraChefao then
    tempoAteAtirar = tempoAteAtirar - (1 * dt)
    else
      tempoAteAtirar = tempoAteAtirar - (2.5 * dt)
    end
    if tempoAteAtirar < 0 then
        atira = true
    end

    if estaVivo then
      if love.keyboard.isDown('q') and atira then
          novoTiro = {x = nave.posX+25, y = nave.posY, img = imgTiro}
          table.insert(tiros, novoTiro)
          somDoTiro:stop()
      	  somDoTiro:play()
          atira = false
        tempoAteAtirar = delayTiro
      end
    end



    for i, tiro in ipairs(tiros) do
        tiro.y = tiro.y - (500 * dt)
        if tiro.y < 0 then
            table.remove(tiros, i)
        end
    end
end

function love.draw()
  if not win then
    --Background
    love.graphics.draw(fundo, planoDeFundo.x, planoDeFundo.y)
    love.graphics.draw(fundo2, planoDeFundo.x, planoDeFundo.y2)
    --Background

    for i, tiro in ipairs(tiros) do
        love.graphics.draw(tiro.img, tiro.x, tiro.y, 0, 1, 1, imgTiro:getWidth() / 2, imgTiro:getHeight())
    end

    --pontos na tela
    love.graphics.setFont(fonte)
    love.graphics.print("Pontuacao: "..pontos, 10, 10)
    --pontos na tela

    --Game over e Reset
    if estaVivo then
        love.graphics.draw(imgNave, nave.posX, nave.posY)
    else
        love.graphics.print("Aperte 'r' para reiniciar", larguraTela / 3, alturaTela /2 )
    end
    --Game over e Reset

    if mostraChefao then
        if tempo > 0 then --tempo pra frase sumir da tela
          for i, inimigo in ipairs ( inimigos ) do
              table.remove(inimigos, i)
            end
          love.graphics.draw(palavras, (larguraTela/2)-100, alturaTela/2)
        end

      if tempo < 0 then
        gateEnemyDraw = true

        love.graphics.draw(chefaoImg, chefao.x, chefao.y)
      end
    end
    --ininmigos
    if gateEnemyDraw then
      for i, inimigo in ipairs (inimigos) do
        love.graphics.draw(inimigo.img, inimigo.x, inimigo.y)
      end
    end
    --inimigos
  else
      love.graphics.draw(winImg, 100, 200)
  end
end

function colisoes()
  for i, inimigo in ipairs(inimigos) do
    for j, tiro in ipairs(tiros) do
      if checaColisao(inimigo.x, inimigo.y, imgInimigo:getWidth(), imgInimigo:getHeight(), tiro.x, tiro.y, imgTiro:getWidth(), imgTiro:getHeight()) then
          table.remove(tiros, j)
          table.remove(inimigos, i)
        --  explodeInimigo:stop()
        --  explodeInimigo:play()
          pontos = pontos + 1
          if pontos == criterioVencer then
            win = true
          end
        end
    end

      --checa morte da nave_her

    if checaColisao(inimigo.x, inimigo.y, imgInimigo:getWidth(), imgInimigo:getHeight(), nave.posX, nave.posY + 20, imgNave:getWidth(), imgNave:getHeight()) and estaVivo
      then
        --( imgNave:getWidth() / 2)
        table.remove(inimigos, i)
        --explodeNave:play()
        estaVivo = false
    end
      --checa morte da nave_hero


    if not gateEnemyDraw then
      table.remove(inimigos, i)
    end
  end
end

function checaColisao(x1, y1, w1, h1, x2, y2, w2, h2)
  return x1 < x2 + w2 and x2 < x1 + w1 and y1 < y2 + h2 and y2 < y1 + h1
end

function reset ()
  if not estaVivo and love.keyboard.isDown('r') then
    tiros = {}
    inimigos = {}

    atira = tempoAteAtirar
    tempoCriarInimigo = delayInimigo

    posX = larguraTela / 2
    posY = alturaTela-90

    pontos = 0
    estaVivo = true
    mostraChefao = false

    tempo = delayTempo

  end
end

function planoDeFundoScrolling (dt)
  planoDeFundo.y = planoDeFundo.y + planoDeFundo.vel * dt
  planoDeFundo.y2 = planoDeFundo.y2 + planoDeFundo.vel * dt

  if planoDeFundo.y > alturaTela then
    planoDeFundo.y = planoDeFundo.y2 - fundo2:getHeight()
  end

  if planoDeFundo.y2 > alturaTela then
    planoDeFundo.y2 = planoDeFundo.y - fundo:getHeight()
  end
end
